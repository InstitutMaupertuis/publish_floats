[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

[![build status](https://gitlab.com/InstitutMaupertuis/publish_floats/badges/melodic/build.svg)](https://gitlab.com/InstitutMaupertuis/publish_floats/commits/melodic)

# Usage
```bash
rosrun publish_floats node _topics:=1
```
