#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <string>
#include <random>
#include <cmath>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "node");
  ros::NodeHandle nh("~");
  std::vector<ros::Publisher> publishers;

  int n_topics(6);
  if (!nh.getParam("topics", n_topics))
    ROS_INFO_STREAM("Number of topics not specified");

  const unsigned number_of_topics(std::abs(n_topics));
  ROS_INFO_STREAM("Publishing " << number_of_topics << " topics");

  for (unsigned i(0); i < number_of_topics; ++i)
    publishers.push_back(nh.advertise<std_msgs::Float32>("topic_" + std::to_string(i), 1, false));

  // Produce seeds
  std::random_device rd;
  std::default_random_engine e1(rd());
  std::uniform_real_distribution<double> uniform_dist_0(1, 10);
  std::uniform_real_distribution<double> uniform_dist_1(0, 1);
  std::vector<double> seeds_0(publishers.size());
  std::vector<double> seeds_1(publishers.size());

  for (auto &seed : seeds_0)
    seed = uniform_dist_0(e1);

  for (auto &seed : seeds_1)
    seed = uniform_dist_1(e1);

  ros::Rate r(30);
  std_msgs::Float32 m;
  m.data = 0;
  double t(0);

  while (nh.ok())
  {
    for (unsigned i(0); i < publishers.size(); ++i)
    {
      m.data = sin(seeds_0.at(i) * t) + (seeds_1.at(i) * t);
      publishers.at(i).publish(m);
    }

    t += 1e6 / (double) r.expectedCycleTime().toNSec();
    r.sleep();
  }

  return 0;
}

